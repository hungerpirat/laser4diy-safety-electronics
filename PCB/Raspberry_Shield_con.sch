EESchema Schematic File Version 2
LIBS:MrBeam_2-0_Assembly-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ADC-DAC_[JS]
LIBS:Amplifier_IC-[JS]
LIBS:burr-brown
LIBS:con-lumberg1
LIBS:conn-Ethernet-[JS]
LIBS:crystals-[TW]
LIBS:inductors_[JS]
LIBS:LED_[JS]
LIBS:mbed
LIBS:Mosfet-[JS]
LIBS:Multiplexer-IC-[JS]
LIBS:pcb_templates-[JS]
LIBS:Power-IC-[JS]
LIBS:Relay-[JS]
LIBS:Sensor-IC-[JS]
LIBS:Systemtechniklabor
LIBS:Transistor-[JS]
LIBS:modules_droids-xbee
LIBS:sw_dip
LIBS:w_analog
LIBS:w_connectors
LIBS:w_device
LIBS:w_logic
LIBS:w_memory
LIBS:w_microcontrollers
LIBS:w_opto
LIBS:w_relay
LIBS:w_rtx
LIBS:w_transistor
LIBS:w_vacuum
LIBS:MrBeam_2-0_Assembly-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5050 6300 0    60   ~ 0
step_1
Text Label 5050 6400 0    60   ~ 0
step_2
Text Label 5050 6500 0    60   ~ 0
step_3
Text Label 5050 6600 0    60   ~ 0
step_4
Text Label 5050 6800 0    60   ~ 0
endstop_x
Text Label 5050 6700 0    60   ~ 0
endstop_y
Text Label 5050 6200 0    60   ~ 0
+5V
Text HLabel 4750 6400 0    60   Output ~ 0
step_2
Text HLabel 4750 6600 0    60   Output ~ 0
step_4
Text HLabel 4750 6300 0    60   Output ~ 0
step_1
Text HLabel 4750 6500 0    60   Output ~ 0
step_3
Text HLabel 4750 6200 0    60   Output ~ 0
+5V
Text HLabel 4750 6700 0    60   Input ~ 0
endstop_y
Text HLabel 4750 6800 0    60   Input ~ 0
endstop_x
$Comp
L GND #PWR014
U 1 1 5702A93D
P 2800 7150
F 0 "#PWR014" H 2800 6900 50  0001 C CNN
F 1 "GND" H 2800 7000 50  0000 C CNN
F 2 "" H 2800 7150 50  0000 C CNN
F 3 "" H 2800 7150 50  0000 C CNN
	1    2800 7150
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR015
U 1 1 5702A93E
P 2800 5950
F 0 "#PWR015" H 2800 5800 50  0001 C CNN
F 1 "VCC" H 2800 6100 50  0000 C CNN
F 2 "" H 2800 5950 50  0000 C CNN
F 3 "" H 2800 5950 50  0000 C CNN
	1    2800 5950
	1    0    0    -1  
$EndComp
Text Label 1850 6400 0    60   ~ 0
Driver_EN
Text Label 1850 6600 0    60   ~ 0
I2C-clk
Text Label 1850 6500 0    60   ~ 0
I2C-data
Text Label 1850 6800 0    60   ~ 0
luefter_GND
Text Label 1850 6900 0    60   ~ 0
GND
Text Label 1850 6200 0    60   ~ 0
diode_pwr
Wire Wire Line
	1650 6200 2800 6200
Wire Wire Line
	2800 6200 2800 5950
Wire Wire Line
	1650 6400 2400 6400
Wire Wire Line
	1650 6500 2400 6500
Wire Wire Line
	1650 6600 2400 6600
Wire Wire Line
	1650 6800 2400 6800
Wire Wire Line
	1650 6900 2800 6900
Wire Wire Line
	2800 6900 2800 7150
Text HLabel 1650 6200 0    60   Input ~ 0
diode_pwr
Text HLabel 1650 6400 0    60   Input ~ 0
driver_EN
Text HLabel 1650 6500 0    60   BiDi ~ 0
I2C-data
Text HLabel 1650 6600 0    60   Input ~ 0
I2C-clk
Text HLabel 1650 6800 0    60   Output ~ 0
luefter_GND
Text HLabel 1650 6900 0    60   Output ~ 0
GND
Text Label 1850 6300 0    60   ~ 0
+12V
Wire Wire Line
	1650 6300 3050 6300
Wire Wire Line
	3050 6300 3050 5950
$Comp
L +12V #PWR016
U 1 1 5702A942
P 3050 5950
F 0 "#PWR016" H 3050 5800 50  0001 C CNN
F 1 "+12V" H 3050 6090 50  0000 C CNN
F 2 "" H 3050 5950 50  0000 C CNN
F 3 "" H 3050 5950 50  0000 C CNN
	1    3050 5950
	1    0    0    -1  
$EndComp
Text HLabel 1650 6300 0    60   Input ~ 0
+12V
Wire Wire Line
	1650 6700 2400 6700
Text Label 1850 6700 0    60   ~ 0
temp
Text HLabel 1650 6700 0    60   Output ~ 0
temp
$Comp
L GND #PWR017
U 1 1 5702B4D4
P 5900 7150
F 0 "#PWR017" H 5900 6900 50  0001 C CNN
F 1 "GND" H 5900 7000 50  0000 C CNN
F 2 "" H 5900 7150 50  0000 C CNN
F 3 "" H 5900 7150 50  0000 C CNN
	1    5900 7150
	1    0    0    -1  
$EndComp
Text Label 5050 6900 0    60   ~ 0
GND
Wire Wire Line
	4750 6900 5900 6900
Wire Wire Line
	5900 6900 5900 7150
Text HLabel 4750 6900 0    60   Input ~ 0
GND
Wire Wire Line
	4750 6200 6200 6200
Wire Wire Line
	4750 6300 5550 6300
Wire Wire Line
	4750 6400 5550 6400
Wire Wire Line
	4750 6500 5550 6500
Wire Wire Line
	4750 6600 5550 6600
Wire Wire Line
	4750 6700 5550 6700
Wire Wire Line
	4750 6800 5550 6800
$Comp
L +5V #PWR018
U 1 1 5702C10E
P 6200 5900
F 0 "#PWR018" H 6200 5750 50  0001 C CNN
F 1 "+5V" H 6200 6040 50  0000 C CNN
F 2 "" H 6200 5900 50  0000 C CNN
F 3 "" H 6200 5900 50  0000 C CNN
	1    6200 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 6200 6200 5900
$Comp
L GND #PWR019
U 1 1 57006717
P 2850 4300
F 0 "#PWR019" H 2850 4050 50  0001 C CNN
F 1 "GND" H 2850 4150 50  0000 C CNN
F 2 "" H 2850 4300 50  0000 C CNN
F 3 "" H 2850 4300 50  0000 C CNN
	1    2850 4300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR020
U 1 1 5700671D
P 2850 3100
F 0 "#PWR020" H 2850 2950 50  0001 C CNN
F 1 "VCC" H 2850 3250 50  0000 C CNN
F 2 "" H 2850 3100 50  0000 C CNN
F 3 "" H 2850 3100 50  0000 C CNN
	1    2850 3100
	1    0    0    -1  
$EndComp
Text Label 1900 3550 0    60   ~ 0
Driver_EN
Text Label 1900 3750 0    60   ~ 0
I2C-clk
Text Label 1900 3650 0    60   ~ 0
I2C-data
Text Label 1900 3950 0    60   ~ 0
luefter_GND
Text Label 1900 4050 0    60   ~ 0
GND
Text Label 1900 3350 0    60   ~ 0
diode_pwr
Wire Wire Line
	1700 3350 2850 3350
Wire Wire Line
	2850 3350 2850 3100
Wire Wire Line
	1700 3550 2450 3550
Wire Wire Line
	1700 3650 2450 3650
Wire Wire Line
	1700 3750 2450 3750
Wire Wire Line
	1700 3950 2450 3950
Wire Wire Line
	1700 4050 2850 4050
Wire Wire Line
	2850 4050 2850 4300
Text Label 1900 3450 0    60   ~ 0
+12V
Wire Wire Line
	1700 3450 3100 3450
Wire Wire Line
	3100 3450 3100 3100
$Comp
L +12V #PWR021
U 1 1 57006734
P 3100 3100
F 0 "#PWR021" H 3100 2950 50  0001 C CNN
F 1 "+12V" H 3100 3240 50  0000 C CNN
F 2 "" H 3100 3100 50  0000 C CNN
F 3 "" H 3100 3100 50  0000 C CNN
	1    3100 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 3850 2450 3850
Text Label 1900 3850 0    60   ~ 0
temp
Text Label 5050 3450 0    60   ~ 0
step_1
Text Label 5050 3550 0    60   ~ 0
step_2
Text Label 5050 3650 0    60   ~ 0
step_3
Text Label 5050 3750 0    60   ~ 0
step_4
Text Label 5050 3950 0    60   ~ 0
endstop_x
Text Label 5050 3850 0    60   ~ 0
endstop_y
Text Label 5050 3350 0    60   ~ 0
+5V
$Comp
L GND #PWR022
U 1 1 57006743
P 5900 4300
F 0 "#PWR022" H 5900 4050 50  0001 C CNN
F 1 "GND" H 5900 4150 50  0000 C CNN
F 2 "" H 5900 4300 50  0000 C CNN
F 3 "" H 5900 4300 50  0000 C CNN
	1    5900 4300
	1    0    0    -1  
$EndComp
Text Label 5050 4050 0    60   ~ 0
GND
Wire Wire Line
	4750 4050 5900 4050
Wire Wire Line
	5900 4050 5900 4300
Wire Wire Line
	4750 3350 6200 3350
Wire Wire Line
	4750 3450 5550 3450
Wire Wire Line
	4750 3550 5550 3550
Wire Wire Line
	4750 3650 5550 3650
Wire Wire Line
	4750 3750 5550 3750
Wire Wire Line
	4750 3850 5550 3850
Wire Wire Line
	4750 3950 5550 3950
$Comp
L +5V #PWR023
U 1 1 57006753
P 6200 3050
F 0 "#PWR023" H 6200 2900 50  0001 C CNN
F 1 "+5V" H 6200 3190 50  0000 C CNN
F 2 "" H 6200 3050 50  0000 C CNN
F 3 "" H 6200 3050 50  0000 C CNN
	1    6200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3350 6200 3050
$Comp
L CONN_01X10 P4
U 1 1 5700675C
P 1500 3700
F 0 "P4" H 1500 4250 50  0000 C CNN
F 1 "CONN_01X10" V 1600 3700 50  0000 C CNN
F 2 "connectors-[JS]:Molex-052207-1533" H 1500 3700 50  0001 C CNN
F 3 "" H 1500 3700 50  0000 C CNN
	1    1500 3700
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X08 P5
U 1 1 5700693B
P 4550 3700
F 0 "P5" H 4550 4150 50  0000 C CNN
F 1 "CONN_01X08" V 4650 3700 50  0000 C CNN
F 2 "connectors-[JS]:Molex-052207-1533" H 4550 3700 50  0001 C CNN
F 3 "" H 4550 3700 50  0000 C CNN
	1    4550 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 3250 1850 3250
Wire Wire Line
	1850 3250 1850 3350
Connection ~ 1850 3350
Wire Wire Line
	1700 4150 1850 4150
Wire Wire Line
	1850 4150 1850 4050
Connection ~ 1850 4050
$EndSCHEMATC
