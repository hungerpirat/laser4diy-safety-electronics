EESchema Schematic File Version 2
LIBS:Raspberry_Shield-rescue
LIBS:PCF8575
LIBS:ac-dc
LIBS:adc-dac
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:brooktre
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:hc11
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:Lattice
LIBS:linear
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:nordicsemi
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:supertex
LIBS:texas
LIBS:video
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:Zilog
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:Atmega8
LIBS:74lvc1g32
LIBS:Raspberry_Shield-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Laser4DIY Raspberry Pi Shield"
Date "2017-12-22"
Rev "01"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 750  700  1500 1500
U 579F3726
F0 "Power_Supply+Safety" 60
F1 "Power_Supply+Safety.sch" 60
$EndSheet
$Sheet
S 750  2575 1500 1500
U 57A1419D
F0 "Interlock circuit" 60
F1 "Interlock_circuit.sch" 60
$EndSheet
$Sheet
S 2500 4350 1500 1500
U 57A1A8FF
F0 "Rasperry-Connector" 60
F1 "Raspberry-Connector.sch" 60
$EndSheet
$Sheet
S 750  4350 1500 1500
U 5806772C
F0 "i2c-expander" 60
F1 "i2c-expander.sch" 60
$EndSheet
$EndSCHEMATC
