EESchema Schematic File Version 4
LIBS:Raspberry_Shield-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Laser4DIY Raspberry Pi Shield"
Date "2017-12-22"
Rev "01"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 750  700  1500 1500
U 579F3726
F0 "Power_Supply+Safety" 60
F1 "Power_Supply+Safety.sch" 60
$EndSheet
$Sheet
S 750  2575 1500 1500
U 57A1419D
F0 "Interlock circuit" 60
F1 "Interlock_circuit.sch" 60
$EndSheet
$Sheet
S 2500 4350 1500 1500
U 57A1A8FF
F0 "Rasperry-Connector" 60
F1 "Raspberry-Connector.sch" 60
$EndSheet
$Sheet
S 750  4350 1500 1500
U 5806772C
F0 "i2c-expander" 60
F1 "i2c-expander.sch" 60
$EndSheet
$EndSCHEMATC
