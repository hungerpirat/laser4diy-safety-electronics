EESchema Schematic File Version 2
LIBS:PCF8575
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:intel
LIBS:interface
LIBS:ir
LIBS:Lattice
LIBS:linear
LIBS:logo
LIBS:maxim
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:motor_drivers
LIBS:motorola
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:Zilog
LIBS:Symbols_DCDC-ACDC-Converter_RevC_20Jul2012
LIBS:Symbols_EN60617_13Mar2013
LIBS:Symbols_EN60617-10_HF-Radio_DRAFT_12Sep2013
LIBS:Symbols_ICs-Diskrete_RevD10
LIBS:Symbols_ICs-Opto_RevB_16Sep2013
LIBS:Symbols_Microcontroller_Philips-NXP_RevA_06Oct2013
LIBS:SymbolsSimilarEN60617+oldDIN617-RevE8
LIBS:Symbols_Socket-DIN41612_RevA
LIBS:Symbols_Transformer-Diskrete_RevA
LIBS:Raspberry_Shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CAT24C32 IC1
U 1 1 57A09EE8
P 4550 3725
F 0 "IC1" H 4825 3400 50  0000 C CNN
F 1 "CAT24C32" H 4950 3275 50  0000 C CNN
F 2 "w_smd_dil:tssop-8" H 4550 3725 50  0001 C CIN
F 3 "" H 4550 3725 50  0000 C CNN
F 4 "CAT24C32YI-GT3" H 4550 3725 60  0001 C CNN "manf#"
	1    4550 3725
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR073
U 1 1 57A0A25F
P 4025 4325
F 0 "#PWR073" H 4025 4075 50  0001 C CNN
F 1 "GND" H 4030 4152 50  0000 C CNN
F 2 "" H 4025 4325 50  0000 C CNN
F 3 "" H 4025 4325 50  0000 C CNN
	1    4025 4325
	1    0    0    -1  
$EndComp
$Comp
L C_Small C4
U 1 1 57A0A4CD
P 4025 3300
F 0 "C4" H 4117 3346 50  0000 L CNN
F 1 "0.1µF" H 4117 3255 50  0000 L CNN
F 2 "w_smd_cap:c_0402" H 4025 3300 50  0001 C CNN
F 3 "" H 4025 3300 50  0000 C CNN
F 4 "Value" H 4025 3300 60  0001 C CNN "manf#"
	1    4025 3300
	1    0    0    -1  
$EndComp
$Comp
L R_Small R2
U 1 1 57A0A7CD
P 3775 3675
F 0 "R2" H 3834 3721 50  0000 L CNN
F 1 "1k" H 3834 3630 50  0000 L CNN
F 2 "w_smd_resistors:r_0402" H 3775 3675 50  0001 C CNN
F 3 "" H 3775 3675 50  0000 C CNN
F 4 "ERJ-2RKF1001X" H 3775 3675 60  0001 C CNN "manf#"
	1    3775 3675
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 57A0A862
P 3575 4100
F 0 "P1" H 3675 3925 50  0000 C CNN
F 1 "JP" H 3700 4100 50  0000 C CNN
F 2 "w_pin_strip:pin_strip_2" H 3575 4100 50  0001 C CNN
F 3 "" H 3575 4100 50  0000 C CNN
F 4 "68001-402HLF" H 3575 4100 60  0001 C CNN "manf#"
	1    3575 4100
	-1   0    0    1   
$EndComp
$Comp
L 3V3_Raspberry #PWR074
U 1 1 57A454DE
P 4550 2900
F 0 "#PWR074" H 4550 2860 30  0001 C CNN
F 1 "3V3_Raspberry" H 4559 3038 30  0000 C CNN
F 2 "" H 4550 2900 60  0000 C CNN
F 3 "" H 4550 2900 60  0000 C CNN
	1    4550 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4025 3400 4025 4325
Wire Wire Line
	4025 3575 4150 3575
Wire Wire Line
	4150 3775 4025 3775
Connection ~ 4025 3775
Wire Wire Line
	4150 3675 4025 3675
Connection ~ 4025 3675
Wire Wire Line
	4550 2900 4550 3375
Connection ~ 4025 3575
Wire Wire Line
	4025 3200 4025 3050
Wire Wire Line
	3775 3050 5350 3050
Connection ~ 4550 3050
Wire Wire Line
	4550 4225 4550 4125
Wire Wire Line
	3775 4225 4550 4225
Connection ~ 4025 4225
Wire Wire Line
	3775 3050 3775 3575
Connection ~ 4025 3050
Wire Wire Line
	4150 3925 3775 3925
Wire Wire Line
	3775 3775 3775 4050
Connection ~ 3775 3925
Wire Wire Line
	3775 4150 3775 4225
Wire Wire Line
	4950 3625 5525 3625
Wire Wire Line
	4950 3775 5525 3775
$Comp
L R_Small R35
U 1 1 57A46115
P 5100 3300
F 0 "R35" H 5159 3346 50  0000 L CNN
F 1 "1k" H 5159 3255 50  0000 L CNN
F 2 "w_smd_resistors:r_0402" H 5100 3300 50  0001 C CNN
F 3 "" H 5100 3300 50  0000 C CNN
	1    5100 3300
	1    0    0    -1  
$EndComp
$Comp
L R_Small R36
U 1 1 57A4619E
P 5350 3300
F 0 "R36" H 5409 3346 50  0000 L CNN
F 1 "1k" H 5409 3255 50  0000 L CNN
F 2 "w_smd_resistors:r_0402" H 5350 3300 50  0001 C CNN
F 3 "" H 5350 3300 50  0000 C CNN
	1    5350 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3400 5350 3775
Connection ~ 5350 3775
Wire Wire Line
	5100 3400 5100 3625
Connection ~ 5100 3625
Wire Wire Line
	5100 3050 5100 3200
Wire Wire Line
	5350 3050 5350 3200
Connection ~ 5100 3050
Text GLabel 5525 3775 2    60   Input ~ 0
ID_SC
Text GLabel 5525 3625 2    60   Input ~ 0
ID_SD
$EndSCHEMATC
